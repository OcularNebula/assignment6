import java.util.Scanner;

public class DayDriver{
	public static void main(String[] args){
		
		Day testDay = new Day();
		Scanner keyboard = new Scanner(System.in);

		System.out.println("Enter a day.");
		String today = keyboard.nextLine();
	
		if(testDay.set(today) != 7){

			System.out.println("How many days forward should I calculate?");
			int amountAdd = keyboard.nextInt();

			System.out.println("Today is " + testDay.get());
			System.out.println("The next day is " + testDay.nextDay());
			System.out.println("The previous day is " + testDay.previousDay());
			System.out.println("The day " + amountAdd + " days from now is " + testDay.addDay(amountAdd));
		}
		else
			System.out.println("Could not find day (check your spelling)");
	}
}
