
public class Day{

	String[] days = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
	int index = 0;

	public int set(String day){
		int errorCount = 0;
		for(int i = 0; i < days.length; i++){
			if(day.equals(days[i]))
				index = i;
			else
				errorCount++;
			}
		return errorCount;
		}

	public void print(){
		System.out.println("Today is " + days[index]);
		}

	public String get(){
		return days[index];
		}

	public String nextDay(){
		int temp = index;
		return days[++temp % days.length];
		}

	public String previousDay(){
		int temp = index;
		return days[(--temp + days.length) % days.length];
		}

	public String addDay(int day){
		int temp = index;
		return days[(temp + day) % days.length];
		}
	}
